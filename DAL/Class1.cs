﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace DAL
{
    static public class DataBase
    {
        static private string path = "../../../DataBase/db.acp";

        static public void SaveContact(Model.Contact contact){
            StreamWriter sw = new StreamWriter(DataBase.path,true);
            sw.WriteLine("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11}", contact.Id,contact.Nom,contact.Prenom,contact.Numero,contact.Email,contact.Naissance.Year,contact.Naissance.Month,contact.Naissance.Day,contact.Adresse.Ville, contact.Adresse.CodePostal, contact.Adresse.NumeroRue, contact.Adresse.Rue);
            sw.Close();
        }

        static public Model.Contact GetContact(uint id)
        {
            StreamReader sr = new StreamReader(DataBase.path);
            string s;
            while ((s = sr.ReadLine()) != null&&getID(s) != id){}
            sr.Close();
            if(s==null)
                return null;
            string[] sa=s.Split(',');
            return new Model.Contact(uint.Parse(sa[0]),sa[1], sa[2], sa[3], sa[4], new Model.Date(int.Parse(sa[5]), int.Parse(sa[6]), int.Parse(sa[7])), new Model.Adresse(sa[8], sa[9], int.Parse(sa[10]),sa[11]));
        }

        static public List<Model.Contact> GetAllContacts()
        {
            List<Model.Contact> list = new List<Model.Contact>();
            string[] sa;
            StreamReader sr = new StreamReader(DataBase.path);
            string s;
            while ((s = sr.ReadLine()) != null)
            {
                sa = s.Split(',');
                list.Add(new Model.Contact(uint.Parse(sa[0]), sa[1], sa[2], sa[3], sa[4], new Model.Date(int.Parse(sa[5]), int.Parse(sa[6]), int.Parse(sa[7])), new Model.Adresse(sa[8], sa[9], int.Parse(sa[10]),sa[11])));
            }
            sr.Close();
            return list;
        }

        static public void DeleteContact(uint id)
        {
            StreamReader sr = new StreamReader(DataBase.path);
            string s;
            List<string> sa = new List<string>();
            while ((s = sr.ReadLine()) != null) {
                if(getID(s) != id)
                    sa.Add(s);
            }
            sr.Close();
            StreamWriter sw = new StreamWriter(DataBase.path);
            foreach(string str in sa)
                sw.WriteLine(str);
            sw.Close();
        }

        static public void ModifyContact(Model.Contact contact)
        {
            StreamReader sr = new StreamReader(DataBase.path);
            string s;
            List<string> sa = new List<string>();
            while ((s = sr.ReadLine()) != null)
            {
                if (getID(s) != contact.Id)
                    sa.Add(s);
                else
                    sa.Add(contact.Id.ToString()+','+contact.Nom + ',' + contact.Prenom + ',' + contact.Numero + ',' + contact.Email + ',' + contact.Naissance.Year + ',' + contact.Naissance.Month + ',' + contact.Naissance.Day + ',' + contact.Adresse.Ville + ',' + contact.Adresse.CodePostal + ',' + contact.Adresse.NumeroRue + ',' + contact.Adresse.Rue);
            }
            sr.Close();
            StreamWriter sw = new StreamWriter(DataBase.path);
            foreach (string str in sa)
                sw.WriteLine(str);
            sw.Close();
        }

        static public uint GetNewID()
        {
            StreamReader sr = new StreamReader(DataBase.path);
            string s;
            uint? id=null;
            while ((s = sr.ReadLine()) != null)
                id = DataBase.getID(s);
            sr.Close();
            if (id == null)
                id = 0;
            else
                id++;
            return (uint)id;
        }

        static private uint getID(string str)
        {
            return uint.Parse(str.Substring(0,str.IndexOf(',')));
        }
    }
}
