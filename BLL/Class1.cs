﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;
using System.IO;
namespace BLL
{
    static public class BLL
    {
        static private List<Contact> list = DAL.DataBase.GetAllContacts();
        static public uint currentId;
        static private uint newID = DAL.DataBase.GetNewID();
        static public string AjoutContact(string nom, string prenom, string numero, string email, string jour, string mois, string annee, string ville, string codepostal, string rue, string numerorue)
        {
            int ijour = 1, imois = 1, iannee = 1, inumerorue = 0;

            if (nom == "" || prenom == "")
                return "Vous devez renseigner un nom et un prénom au minimum";

            if (nom.Contains(',') || prenom.Contains(',') || email.Contains(',') || ville.Contains(',') || codepostal.Contains(',') || rue.Contains(','))
                return "Aucun champ ne doit comporter de virgule";

            if (numero != "" && !long.TryParse(numero, out long n))
                return "Le numéro de téléphone ne doit comporter que des chiffres";
            if (email != "" && (!email.Contains('@') || !email.Contains('.')))
                return "L'adresse email doit contenir au moins un '@' et un '.'";
            if (annee != "" && !int.TryParse(annee, out iannee))
                return "L'année ne doit comporter que des chiffres";
            if (iannee < 1 || imois > 9999)
                return "L'année doit être comprise entre 1 et 9999";
            if (mois != "" && !int.TryParse(mois, out imois))
                return "Le mois ne doit comporter que des chiffres";
            if (imois < 1 || imois > 12)
                return "Le mois doit être compris entre 1 et 12";
            if (jour != "" && !int.TryParse(jour, out ijour))
                return "Le jour ne doit comporter que des chiffres";
            if (ijour < 1 || ijour > DateTime.DaysInMonth(iannee, imois))
                return "Le jour doit être compris entre 1 et " + DateTime.DaysInMonth(iannee, imois);
            if (numerorue != "" && !int.TryParse(numerorue, out inumerorue))
                return "Le numéro de rue ne doit comporter que des chiffres";

            BLL.list.Add(new Contact(BLL.newID++, nom, prenom, numero, email, new Date(iannee, imois, ijour), new Adresse(ville, codepostal, inumerorue, rue)));
            DAL.DataBase.SaveContact(BLL.list.Last());
            return null;
        }

        static public string EditionContact(string nom, string prenom, string numero, string email, string jour, string mois, string annee, string ville, string codepostal, string rue, string numerorue)
        {
            int ijour = 1, imois = 1, iannee = 1, inumerorue = 0;

            if (nom == "" || prenom == "")
                return "Vous devez renseigner un nom et un prénom au minimum";

            if (nom.Contains(',') || prenom.Contains(',') || email.Contains(',') || ville.Contains(',') || codepostal.Contains(',') || rue.Contains(','))
                return "Aucun champ ne doit comporter de virgule";

            if (numero != "" && !long.TryParse(numero, out long n))
                return "Le numéro de téléphone ne doit comporter que des chiffres";
            if (email != "" && (!email.Contains('@') || !email.Contains('.')))
                return "L'adresse email doit contenir au moins un '@' et un '.'";
            if (annee != "" && !int.TryParse(annee, out iannee))
                return "L'année ne doit comporter que des chiffres";
            if (iannee < 1 || imois > 9999)
                return "L'année doit être comprise entre 1 et 9999";
            if (mois != "" && !int.TryParse(mois, out imois))
                return "Le mois ne doit comporter que des chiffres";
            if (imois < 1 || imois > 12)
                return "Le mois doit être compris entre 1 et 12";
            if (jour != "" && !int.TryParse(jour, out ijour))
                return "Le jour ne doit comporter que des chiffres";
            if (ijour < 1 || ijour > DateTime.DaysInMonth(iannee, imois))
                return "Le jour doit être compris entre 1 et " + DateTime.DaysInMonth(iannee, imois);
            if (numerorue != "" && !int.TryParse(numerorue, out inumerorue))
                return "Le numéro de rue ne doit comporter que des chiffres";

            Contact contact = new Contact(BLL.currentId, nom, prenom, numero, email, new Date(iannee, imois, ijour), new Adresse(ville, codepostal, inumerorue, rue));
            int i = 0;
            while (BLL.list[i].Id != contact.Id) { i++; }
            BLL.list[i] = contact;
            DAL.DataBase.ModifyContact(contact);
            return null;
        }
        static public void SuppressionContact(uint id)
        {
            int i = 0;
            while (BLL.list[i].Id != id) { i++; }
            BLL.list.RemoveAt(i);
            DAL.DataBase.DeleteContact(id);
        }
        static public List<Contact> AffichageContacts()
        {
            return BLL.list;
        }
        static public List<Contact> RechercheContacts(string critere, string value)
        {
            List<Contact> l = new List<Contact>();
            switch (critere)
            {
                case "Nom":
                    foreach (Contact c in BLL.list)
                        if (c.Nom.ToLower() == value.ToLower())
                            l.Add(c);
                    break;
                case "Prénom":
                    foreach (Contact c in BLL.list)
                        if (c.Prenom.ToLower() == value.ToLower())
                            l.Add(c);
                    break;
                case "Ville":
                    foreach (Contact c in BLL.list)
                        if (c.Adresse.Ville.ToLower() == value.ToLower())
                            l.Add(c);
                    break;
                case "Date de naissance":
                    foreach (Contact c in BLL.list)
                        if (c.Naissance.ToString() == value)
                            l.Add(c);
                    break;
                case "id":
                    foreach (Contact c in BLL.list)
                        if (c.Id == uint.Parse(value))
                            l.Add(c);
                    break;
            }
            return l;
        }
        static public List<Contact> TriContacts(string critere)
        {
            List<Contact> l = new List<Contact>(BLL.list);
            switch (critere)
            {
                case "Nom":
                    l.Sort(BLL.CompareContactsByLastName);
                    break;
                case "Prénom":
                    l.Sort(BLL.CompareContactsByFirstName);
                    break;
                case "Ville":
                    l.Sort(BLL.CompareContactsByCity);
                    break;
                case "Date de naissance":
                    l.Sort(BLL.CompareContactsByDOB);
                    break;
                case "id":
                    l.Sort(BLL.CompareContactsByID);
                    break;
            }
            return l;
        }
        static private int CompareContactsByLastName(Contact x, Contact y)
        {
            return x.Nom.CompareTo(y.Nom);
        }
        static private int CompareContactsByFirstName(Contact x, Contact y)
        {
            return x.Prenom.CompareTo(y.Prenom);
        }
        static private int CompareContactsByCity(Contact x, Contact y)
        {
            return x.Adresse.Ville.CompareTo(y.Adresse.Ville);
        }
        static private int CompareContactsByDOB(Contact x, Contact y)
        {
            return x.Naissance.ToInt() - y.Naissance.ToInt();
        }
        static private int CompareContactsByID(Contact x, Contact y)
        {
            return (int)(x.Id - y.Id);
        }
        /*static private bool VerifyName(string str)
        /// Returns true if a string contains only letters
        /// Returns false otherwise
        {
            char[] charArr = str.ToCharArray();
            uint u;
            foreach (char c in charArr)
            {
                u = (uint)c;
                if (u < (uint)0xfeff0041*//* || u > 0xfeff005a && u < 0xfeff0061 || u > 0xfeff007a && u < 0xfeff00c0 || u > 0xfeff017e*//*)
                    return false;
            }
            return true;
        }*/
    }
}
