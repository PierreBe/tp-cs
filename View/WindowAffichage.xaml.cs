﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Model;

namespace View
{
    /// <summary>
    /// Interaction logic for WindowAffichage.xaml
    /// </summary>
    public partial class WindowAffichage : Window
    {
        public void Refresh()
        {
            liste.Items.Clear();
            List<Contact> l = BLL.BLL.AffichageContacts();
            foreach (Contact t in l)
            {
                liste.Items.Add(t);
            }
        }
        public WindowAffichage()
        {
            InitializeComponent();
        }
        private void button_Click(object sender, RoutedEventArgs e)
        {
            string text = ((Button)sender).Content.ToString();
            if (text == "Retour")
            {
                this.Visibility = System.Windows.Visibility.Hidden;
                MainWindow.wl[(int)WLE.MAIN].Visibility = System.Windows.Visibility.Visible;
            }

        }
        private void DataWindow_Closing(object sender, CancelEventArgs e)
        {
            Environment.Exit(0);
        }
    }
}
