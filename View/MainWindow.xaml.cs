﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
namespace View
{
    enum WLE
    {
        MAIN, AJOUTER, EDITER, RECHERCHER, AFFICHER, TRIER
    }
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        static internal List<Window> wl;
        static private void WLInit(Window win)
        {
            MainWindow.wl = new List<Window>();
            MainWindow.wl.Add(win);
            MainWindow.wl.Add(new Window1());
            MainWindow.wl.Add(new WindowEditer());
            MainWindow.wl.Add(new WindowRecherche());
            MainWindow.wl.Add(new WindowAffichage());
            MainWindow.wl.Add(new WindowTri());
        }
        public MainWindow()
        {
            MainWindow.WLInit(this);
            InitializeComponent();
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            int ind = 0;
            switch (((Button)sender).Name.ToString())
            {
                case "btn_affichage":
                    ind = (int)WLE.AFFICHER;
                    ((WindowAffichage)MainWindow.wl[ind]).Refresh();
                    break;
                case "btn_ajout":
                    ind = (int)WLE.AJOUTER;
                    break;
                case "btn_edition":
                    ind = (int)WLE.EDITER;
                    ((WindowEditer)MainWindow.wl[ind]).Refresh();
                    break;
                case "btn_recherche":
                    ind = (int)WLE.RECHERCHER;
                    break;
                case "btn_tri":
                    ind = (int)WLE.TRIER;
                    break;
                default:
                    break;
            }
            this.Visibility = System.Windows.Visibility.Hidden;
            MainWindow.wl[ind].Visibility = System.Windows.Visibility.Visible;
        }

        private void DataWindow_Closing(object sender, CancelEventArgs e)
        {
            Environment.Exit(0);
        }
    }
}