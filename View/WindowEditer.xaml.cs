﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Model;
namespace View
{
    /// <summary>
    /// Interaction logic for WindowEditer.xaml
    /// </summary>
    public partial class WindowEditer : Window
    {
        public void Refresh()
        {
            liste.Items.Clear();
            this.l = BLL.BLL.AffichageContacts();
            foreach (Contact t in l)
            {
                liste.Items.Add(t);
            }
        }
        private List<Contact> l;
        public WindowEditer()
        {
            InitializeComponent();
            /*this.l = BLL.BLL.AffichageContacts();
            string ligne = "";
            foreach (Contact t in l)
            {
                liste.Items.Add(ligne);
            }*/
        }
        private void button_Click(object sender, RoutedEventArgs e)
        {
            int ind = liste.SelectedIndex;
            string text = ((Button)sender).Name.ToString();
            if (text == "btn_editer"&&ind!=-1)
            {
                ((Window1)MainWindow.wl[(int)WLE.AJOUTER]).Fill(this.l[ind]);
                BLL.BLL.currentId=this.l[ind].Id;

                this.Visibility = System.Windows.Visibility.Hidden;
                MainWindow.wl[(int)WLE.AJOUTER].Visibility = System.Windows.Visibility.Visible;
            }
            else if (text == "btn_supprimer" && ind != -1)
            {
                BLL.BLL.SuppressionContact(this.l[ind].Id);
                this.Refresh();
            }
            else if (text == "btn_retour")
            {
                this.Visibility = System.Windows.Visibility.Hidden;
                MainWindow.wl[(int)WLE.MAIN].Visibility = System.Windows.Visibility.Visible;
            }
        }

        private void DataWindow_Closing(object sender, CancelEventArgs e)
        {
            Environment.Exit(0);
        }
    }
}
