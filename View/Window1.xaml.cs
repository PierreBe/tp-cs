﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace View
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {
        public void Fill(Model.Contact contact)
        {
            this.Title = "Édition de contact";
            this.titre.Content = "Édition de contact";
            btn_ajouter.Content = "Modifier";
            nom.Text = contact.Nom;
            prenom.Text = contact.Prenom;
            telephone.Text = contact.Numero.ToString();
            email.Text = contact.Email;
            noRue.Text = "" + contact.Adresse.NumeroRue;
            rue.Text = contact.Adresse.Rue;
            codePostal.Text = contact.Adresse.CodePostal;
            ville.Text = contact.Adresse.Ville;
            annee.Text = "" + contact.Naissance.Year;
            jour.Text = "" + contact.Naissance.Day;
            mois.Text = "" + contact.Naissance.Month;
        }
        public void Empty()
        {
            nom.Clear();
            prenom.Clear();
            telephone.Clear();
            email.Clear();
            noRue.Clear();
            rue.Clear();
            codePostal.Clear();
            ville.Clear();
            annee.Clear();
            mois.Clear();
            jour.Clear();
        }
        public Window1()
        {
            InitializeComponent();
        }
        private void button_Click(object sender, RoutedEventArgs e)
        {
            string text = ((Button)sender).Content.ToString();
            if (text == "Ajouter")
            {
                string str = BLL.BLL.AjoutContact(nom.Text, prenom.Text, telephone.Text, email.Text, jour.Text, mois.Text, annee.Text, ville.Text, codePostal.Text, rue.Text, noRue.Text);
                if (str == null)
                {
                    Empty();
                    System.Windows.Forms.MessageBox.Show("Contact ajouté", "Succès");
                }
                else
                {
                    System.Windows.Forms.MessageBox.Show(str, "Erreur");
                }

            }
            else if (text == "Modifier")
            {
                string str = BLL.BLL.EditionContact(nom.Text, prenom.Text, telephone.Text, email.Text, jour.Text, mois.Text, annee.Text, ville.Text, codePostal.Text, rue.Text, noRue.Text);
                if (str == null)
                {
                    ((WindowEditer)MainWindow.wl[(int)WLE.EDITER]).Refresh();
                    this.Visibility = System.Windows.Visibility.Hidden;
                    MainWindow.wl[(int)WLE.EDITER].Visibility = System.Windows.Visibility.Visible;
                    Empty();
                    this.Title = "Ajout de contact";
                    this.titre.Content = "Ajout de contact";
                    btn_ajouter.Content = "Ajouter";
                    System.Windows.Forms.MessageBox.Show("Contact modifié", "Succès");
                }
                else
                {
                    System.Windows.Forms.MessageBox.Show(str, "Erreur");
                }
            }
            else if (text == "Retour")
            {
                this.Visibility = System.Windows.Visibility.Hidden;
                if (this.btn_ajouter.Content.Equals("Modifier"))
                {
                    MainWindow.wl[(int)WLE.EDITER].Visibility = System.Windows.Visibility.Visible;
                    Empty();
                    this.Title = "Ajout de contact";
                    this.titre.Content = "Ajout de contact";
                    btn_ajouter.Content = "Ajouter";
                }
                else
                {
                    MainWindow.wl[(int)WLE.MAIN].Visibility = System.Windows.Visibility.Visible;
                }
            }

        }

        private void DataWindow_Closing(object sender, CancelEventArgs e)
        {
            Environment.Exit(0);
        }
    }
}
