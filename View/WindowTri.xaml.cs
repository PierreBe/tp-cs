﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace View
{
    /// <summary>
    /// Interaction logic for WindowTri.xaml
    /// </summary>
    public partial class WindowTri : Window
    {
        public WindowTri()
        {
            InitializeComponent();
            this.comboBox.Items.Add("Nom");
            this.comboBox.Items.Add("Prénom");
            this.comboBox.Items.Add("Ville");
            this.comboBox.Items.Add("Date de naissance");
            this.comboBox.Items.Add("id");
        }
        private void button_Click(object sender, RoutedEventArgs e)
        {
            string text = ((Button)sender).Name.ToString();
            if (text == "btn_trier")
            {
                this.liste.Items.Clear();
                List<Model.Contact> l = BLL.BLL.TriContacts(this.comboBox.SelectedItem.ToString());
                foreach (Model.Contact c in l)
                    this.liste.Items.Add(c);
            }
            else if (text == "btn_retour")
            {
                
                this.Visibility = System.Windows.Visibility.Hidden;
                MainWindow.wl[(int)WLE.MAIN].Visibility = System.Windows.Visibility.Visible;
            }
        }

        private void DataWindow_Closing(object sender, CancelEventArgs e)
        {
            Environment.Exit(0);
        }
    }

}
