﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Contact
    {
        private uint id;
        public uint Id { get { return this.id; } }
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public string Numero { get; set; }
        public string Email { get; set; }
        public Date Naissance { get; set; }
        public Adresse Adresse { get; set; }
        public Contact(uint id, string nom, string prenom, string numero, string email, Date naissance, Adresse adresse)
        {
            this.id = id;
            this.Nom = nom;
            this.Prenom = prenom;
            this.Numero = numero;
            this.Email = email;
            this.Naissance = naissance;
            this.Adresse = adresse;
        }
    }

    public class Adresse
    {
        public string Ville { get; set; }
        public string CodePostal { get; set; }
        public string Rue { get; set; }
        public int NumeroRue { get; set; }
        public Adresse(string ville, string codePostal, int numerorue, string rue)
        {
            this.Ville = ville;
            this.CodePostal = codePostal;
            this.NumeroRue = numerorue;
            this.Rue = rue;
        }
        override public string ToString()
        {
            return this.NumeroRue.ToString() + ' ' + this.Rue + ' ' + this.CodePostal + ' ' + this.Ville;
        }

    }

    public class Date
    {
        private DateTime date;
        public int Year { get { return this.date.Year; } }
        public int Month { get { return this.date.Month; } }
        public int Day { get { return this.date.Day; } }
        public Date(int year, int month, int day)
        {
            this.date = new DateTime(year, month, day);
        }
        override public string ToString()
        {
            return this.date.Day.ToString() + '/' + this.date.Month + '/' + this.date.Year;
        }
        public int ToInt()
        {
            return int.Parse(this.date.Year.ToString()+(this.date.Month<10? "0"+this.date.Month: this.date.Month.ToString()) + (this.date.Day < 10 ? "0" + this.date.Day : this.date.Day.ToString()));
        }
    }
}
